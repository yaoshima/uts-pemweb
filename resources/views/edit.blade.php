@extends('layout.master')

@section('main')
    <div id="app" class="min-h-screen" style="background-color: #334d50;">
        @include('partials.nav')
        
        <main class="container mt-10">
            <div class="flex">
                <!-- left -->
                <div class="w-3/5 mr-2">
                    <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" id="editProfile" method="POST" action="{{route('update.profile',['user' => $user->username])}}">
                        {{csrf_field()}}
                        <input name="_method" type="hidden" value="PUT">
                        <div class="mb-2">
                            <label class="block text-grey-darker text-sm font-bold mb-2">
                            Full name
                            </label>
                            <input value="{{$user->fullname}}" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="fullname" type="text" placeholder="Fullname" name="fullname">
                        </div>
                        <div class="mb-2">
                            <label class="block text-grey-darker text-sm font-bold mb-2">
                            Bio 
                            </label>
                            <textarea rows="4" cols="50" id="bio" form="editProfile" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" placeholder="Bio" name="bio">{{$user->bio}}</textarea>
                        </div>
                        <div class="mb-2">
                            <label class="block text-grey-darker text-sm font-bold mb-2">
                            Location 
                            </label>
                            <input value="{{$user->location}}" class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="location" type="text" placeholder="Location" name="location">
                        </div>
                        <div class="mb-2">
                            <label class="block text-grey-darker text-sm font-bold mb-2">
                            Email 
                            </label>
                            <input disabled class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="email" type="text" placeholder="Email" value="{{$user->email}}">
                        </div>
                        <div class="mb-2">
                            <label class="block text-grey-darker text-sm font-bold mb-2">
                            Username 
                            </label>
                            <input disabled class="shadow appearance-none border rounded w-full py-2 px-3 text-grey-darker leading-tight focus:outline-none focus:shadow-outline" id="email" type="text" placeholder="Username" value="{{$user->username}}">
                        </div>
                        <div class="mb-2">
                            <label class="block text-grey-darker text-sm font-bold mb-2" for="username">
                            Photo
                            </label>
                            
                        </div>
                        <div class="flex">
                            <div class="w-3/3 mr-2">
                                <button type="submit" class="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                                    Save
                                </button>
                            </div>
                            <div class="flex-1 ml-2">
                                <button class="bg-grey hover:bg-grey-dark text-grey-darker font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="button">
                                    Cancel
                                </button>
                            </div>
                        </div>
                        
                       
                    </form>
                </div>
                    

                <!-- right -->
                <div class="flex-1">
                    
                </div>
            </div>
        </main>

@endsection



