@extends('layout.master')

@section('main')
    <div id="app" class="min-h-screen" style="background-color: #334d50;">
        @include('partials.nav')
        
        <main class="container mt-6">
            <div class="flex">
                <!-- left -->
                <div class="flex-1 mb-4">
                    <!-- sending post card -->
                    <div class="rounded p-4" style="background-color: rgba(0,0,0,0.3);">

                        <p class="text-xs text-white mb-2 post-box-text">Post Here</p>
                        <form action="{{route('create.post')}}" method="POST">
                          {{csrf_field()}}
                          <textarea placeholder="What, when, why… ask" rows="2" class="w-full rounded p-2" name="content"></textarea>
                          <div class="flex justify-between items-center">
                              <!-- Rounded switch -->
                              <div class="flex items-center">
                                  <label class="switch mr-2">
                                    <input type="checkbox" name="anonymous">
                                    <span class="slider round"></span>

                                  </label>
                                  <span class="text-xs text-white">Ask anonymously</span>
                              </div>
                              

                              <button class="text-white w-1/4 bg-red p-1 rounded mt-2 flex justify-center items-center">
                                 <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 10L0 20V0zm0 8v4l10-2L0 8z"/></svg>
                              </button>
                          </div>
                        </form>
                    </div>

                    @foreach($posts as $post)
                      <!-- post card -->
                      <div class="bg-white rounded p-4 mt-4">
                          <!-- question -->
                          <div class="mb-2">
                              <p class="inline post-card-text  text-lg">{{$post->content}}</p>
                              <!-- sender -->
                              <span class="inline-flex items-center">
                                  
                                  
                                  @if($post->anonymous)
                                    <img src="https://4.bp.blogspot.com/-uC71XXPKXc4/VzMtGXLpFmI/AAAAAAAAT-s/_rqnKX4ubXE6ggVKTjHfb9cmJBFKeUtiwCLcB/s1600/HiResblackhat.jpg"  class=" w-5 h-5 border rounded-full mr-1">
                                    <span class="text-xs">Anonymous</span>
                                    
                                  @else
                                    <img src="https://placekitten.com/15/15"  class="rounded-full mr-1 w-5 h-5 border">
                                    <span class="text-xs">{{$post->user->fullname}}</span>
                                  @endif
                                  
                              </span>
                          </div>

                          @if($post->replies->count())

                            @if($post->replies->first()->anonymous)
                              <!-- answer -->
                              <div class="mt-4">
                                  <!-- recipient -->
                                  <div class="flex">
                                     <img src="https://via.placeholder.com/30" class="rounded-full mr-2" style="height: 30px; width: 30px;">
                                     <div class="">
                                         <p class="text-xs">Anonymous</p> 
                                         <p class="text-grey text-xs">{{$post->replies->first()->created_at}}</p>
                                     </div>
                                  </div>
                                  <!-- response -->
                                  <p class="mt-4">
                                      {{$post->replies->first()->content}}
                                  </p>
                                  
                              </div>
                              
                            @else
                              <!-- answer -->
                              <div class="mt-4">
                                  <!-- recipient -->
                                  <div class="flex">
                                     <img src="https://via.placeholder.com/30" class="rounded-full mr-2" style="height: 30px; width: 30px;">
                                     <div class="">
                                         <p class="text-xs">{{$post->replies->first()->user->fullname}}</p> 
                                         <p class="text-grey text-xs">{{$post->replies->first()->created_at}}</p>
                                     </div>
                                  </div>
                                  <!-- response -->
                                  <p class="mt-4">
                                      {{$post->replies->first()->content}}
                                  </p>
                                  
                              </div>
                            @endif

                            
                          @endif
                          

                          <!-- bottom menubar -->
                          <div class="flex  mt-4">
                              <div class="text-red pr-4 flex items-center">
                                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 3.22l-.61-.6a5.5 5.5 0 0 0-7.78 7.77L10 18.78l8.39-8.4a5.5 5.5 0 0 0-7.78-7.77l-.61.61z"/></svg>
                                  <span class="ml-1 text-red">7</span>
                              </div>
                              <div class="text-grey cursor-pointer" @click="toggleModal('{{$post->id}}')">
                                  <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 10L0 20V0zm0 8v4l10-2L0 8z"/></svg>
                              </div>
                          </div>

                          @if($post->replies->count())
                            <hr class="border-t mx-2 border-grey-ligght">
                            <!-- more -->
                            <div class="flex justify-between">
                                <p class="text-xs text-red-light">+{{$post->replies->count()}} more answer</p>
                                <p>
                                  <a class="text-xs text-grey no-underline" href="{{route('show.post',['post' => $post->id])}}">Read more</a>
                                </p>
                            </div>
                          @endif

                          
                      </div>
                    @endforeach

                    <!-- post card -->
                    <!-- <div class="bg-white rounded p-4 mt-4">
                        question
                        <div class="mb-2">
                            <p class="inline post-card-text  text-lg">ngapain sih kalau abis putus ama pacar?</p>
                            sender
                            <span class="inline-flex items-center">
                                <img src="https://placekitten.com/15/15"  class="rounded-full mr-1">
                                <span class="text-xs">Billy</span>
                            </span>
                        </div>
                        
                        
                        answer
                        <div class="mt-4">
                            recipient
                            <div class="flex">
                               <img src="https://via.placeholder.com/30" class="rounded-full mr-2" style="height: 30px; width: 30px;">
                               <div class="">
                                   <p class="text-xs">aisyahqaulan</p> 
                                   <p class="text-grey text-xs">about 1 hour ago</p>
                               </div>
                            </div>
                            response
                            <p class="mt-4">
                                Tenang, aku masih punya kancing dan penghapus untuk mengisi pg serta khalayan indah untuk mengisi essay
                            </p>
                            
                        </div>
                    
                        bottom menubar
                        <div class="flex  mt-4">
                            <div class="text-red pr-4 flex items-center">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 3.22l-.61-.6a5.5 5.5 0 0 0-7.78 7.77L10 18.78l8.39-8.4a5.5 5.5 0 0 0-7.78-7.77l-.61.61z"/></svg>
                                <span class="ml-1 text-red">7</span>
                            </div>
                            <div class="text-grey cursor-pointer" @click="toggleModal">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 10L0 20V0zm0 8v4l10-2L0 8z"/></svg>
                            </div>
                        </div>
                    
                        <hr class="border-t mx-2 border-grey-ligght">
                        more
                        <div class="flex justify-between">
                            <p class="text-xs text-red-light">+1 more answer</p>
                            <p class="text-xs text-grey">Read more</p>
                        </div>
                    </div> -->


                </div>

                <!-- right -->
                <div class="flex-1">
                    
                </div>
            </div>
        </main>



        <div v-show="modal" @click.self="toggleModal" class="invisible modal fixed z-50 pin overflow-hidden flex" style="background-color: rgba(31,21,20,0.8);">
          <div class="animated fadeInUp fixed shadow-inner max-w-md md:relative pin-b pin-x align-top m-auto justify-end md:justify-center p-8 bg-white md:rounded w-full md:h-auto md:shadow flex flex-col">
            <p class="">Membalas Pertanyaan</p>
            <p class="mb-2 text-xs text-grey-darker">
                in “Pernah gak sih ngerasain tiba2 sedih,galau,pengannya nangis aja gitu. Tapi gak ada terjadi hal apa2 . Yang kamu lakuin apa?”
            </p>
            <form action="{{route('create.reply')}}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="parent" id="parent">
                <textarea placeholder="Replying" rows="2" class="w-full border rounded p-2" name="content"></textarea>
                 <div class="flex justify-between items-center mt-2">
                     <!-- Rounded switch -->
                     <div class="flex items-center">
                         <label class="switch mr-2" >
                           <input type="checkbox" name="anonymous">
                           <span class="slider round"></span>

                         </label>
                         <span class="text-xs text-black">Ask anonymously</span>
                     </div>
                     

                     <button class="text-white w-1/6 bg-red p-1 rounded flex justify-center items-center">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 10L0 20V0zm0 8v4l10-2L0 8z"/></svg>
                     </button>
                 </div>
            </form>
            <span @click="toggleModal" class="absolute pin-t pin-r pt-4 px-4">
                <svg class="h-6 w-6 text-grey hover:text-grey-darkest" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
            </span>
          </div>
        </div>




    </div>
    

@endsection



