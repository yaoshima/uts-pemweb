<div class="shadow" style="background-image: linear-gradient(to bottom, #22292f, #334d50 );">
    <div class="container">
        <!-- nav -->
        <nav class="flex justify-between items-center p-2 ">
            <!-- brand -->
            <span>
                <a href="#" class="text-grey">Suara UMN</a>
            </span>

            @guest
              <div class="flex items-center " style="color: rgba(255,255,255,0.5);">
                <div class="p-2 hover:bg-red text-red-light font-bold hover:text-white">
                  <a class="no-underline" style="color: inherit;" href="{{route('show.register')}}">Register</a>
                </div>
                <div class="p-2 hover:bg-red text-red-light font-bold hover:text-white">
                  <a class="no-underline" style="color: inherit;" href="{{route('show.login')}}">Login</a>
                </div>
              </div>
            @endguest
            
            @auth
              <!-- menu -->
              <div class="flex items-center" style="color: rgba(255,255,255,0.5);">
                  <a href="{{route('home')}}" style="color: inherit;" class="px-6">
                      
                      
                      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M8 20H3V10H0L10 0l10 10h-3v10h-5v-6H8v6z"/></svg>
                  </a>
                  <a href="{{route('show.user',['user' => Auth::user()->username])}}" style="color: inherit;" class="px-6">
                      
                      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zM7 6v2a3 3 0 1 0 6 0V6a3 3 0 1 0-6 0zm-3.65 8.44a8 8 0 0 0 13.3 0 15.94 15.94 0 0 0-13.3 0z"/></svg>
                  </a>
                  <a href="#" style="color: inherit;" class="px-6">
                      
                      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M7 8a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0 1c2.15 0 4.2.4 6.1 1.09L12 16h-1.25L10 20H4l-.75-4H2L.9 10.09A17.93 17.93 0 0 1 7 9zm8.31.17c1.32.18 2.59.48 3.8.92L18 16h-1.25L16 20h-3.96l.37-2h1.25l1.65-8.83zM13 0a4 4 0 1 1-1.33 7.76 5.96 5.96 0 0 0 0-7.52C12.1.1 12.53 0 13 0z"/></svg>
                  </a>
                  
                  <div class="relative group px-6">
                    <div @click="toggleDropdown" class="flex items-center cursor-pointer py-1 px-2">


                      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M3.94 6.5L2.22 3.64l1.42-1.42L6.5 3.94c.52-.3 1.1-.54 1.7-.7L9 0h2l.8 3.24c.6.16 1.18.4 1.7.7l2.86-1.72 1.42 1.42-1.72 2.86c.3.52.54 1.1.7 1.7L20 9v2l-3.24.8c-.16.6-.4 1.18-.7 1.7l1.72 2.86-1.42 1.42-2.86-1.72c-.52.3-1.1.54-1.7.7L11 20H9l-.8-3.24c-.6-.16-1.18-.4-1.7-.7l-2.86 1.72-1.42-1.42 1.72-2.86c-.3-.52-.54-1.1-.7-1.7L0 11V9l3.24-.8c.16-.6.4-1.18.7-1.7zM10 13a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/></svg>
                      
                    </div>
                    <div v-show="isDropdownOpen" class="dropdown-item invisible"  style="right: 0; top: 30px;">
                     <a href="#" class="px-4 py-2 block text-black hover:bg-grey-lighter">View Profile</a>
                     <a href="#" class="px-4 py-2 block text-black hover:bg-grey-lighter">Edit Profile</a>
                     <hr class="border-t mx-2 border-grey-ligght">
                     <a href="{{route('logout')}}" class="px-4 py-2 block text-black hover:bg-grey-lighter">Logout</a>
                    </div>
                  </div>
              </div>
            @endauth
            
        </nav>
    </div>
</div>