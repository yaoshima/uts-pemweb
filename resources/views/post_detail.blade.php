@extends('layout.master')

@section('main')
    <div id="app" class="min-h-screen" style="background-image: linear-gradient(to bottom, #ef5753 , #334d50);">
        @include('partials.nav')
        
        <main class="container mt-10 pb-6" >
           
            <div class="flex">
                <!-- left -->
                <div class="w-3/5 mr-2">
                  <!-- original post -->
                  <div>
                    <!-- header -->
                    <div class="flex">

                      <div class="pr-4 flex items-center" style="color: #00aab8">
                          <svg class="fill-current h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-4 4v-4H2a2 2 0 0 1-2-2V3c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-8zM5 7v2h2V7H5zm4 0v2h2V7H9zm4 0v2h2V7h-2z"/></svg>
                          <span class="ml-1 text-red">{{$post->replies->count() + 1}}</span>
                      </div>

                      <div class="text-red pr-4 flex items-center">
                          <svg class="fill-current h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 3.22l-.61-.6a5.5 5.5 0 0 0-7.78 7.77L10 18.78l8.39-8.4a5.5 5.5 0 0 0-7.78-7.77l-.61.61z"/></svg>
                          <span class="ml-1 text-red">7</span>
                      </div>

                      <p class="text-white text-xs">
                        {{$post->created_at}}
                      </p>
                    </div>

                    <!-- question -->
                    <p class="text-white text-xl font-bold mt-4">{{$post->content}}</p>
                  </div>

                  @if($post->replies->count())
                    @foreach($post->replies as $reply)
                      
                      <!-- response -->
                      <div class="p-4 mt-4 rounded" style="background-color : rgba(0,0,0,0.3); ">
                        <!-- user header -->
                        <div class="flex items-start mb-6">
                          <div class="flex">
                            @if($reply->anonymous)
                              <img src="https://4.bp.blogspot.com/-uC71XXPKXc4/VzMtGXLpFmI/AAAAAAAAT-s/_rqnKX4ubXE6ggVKTjHfb9cmJBFKeUtiwCLcB/s1600/HiResblackhat.jpg"  class="w-10 h-10 shadow-outline rounded-full mr-1">
                              <div class="ml-5">
                                <p class="text-xs text-white mb-1">Post by</p>
                                <p class="text-white tracking-wide font-bold text-md" >Anonymous</p>
                              </div>
                            @else
                              <img src="https://placekitten.com/80/80"  class="w-10 h-10 shadow-outline rounded-full mr-1">
                              <div class="ml-5">
                                <p class="text-xs text-white mb-1">Post by</p>
                                <p class="text-white tracking-wide font-bold text-md" >{{$reply->user->username}}</p>
                              </div>
                            @endif

                          </div>
                          <p class="text-xs text-grey-lighter flex-1 text-right">{{$reply->created_at}}</p>
                        </div>

                        <!-- user content -->
                        <div class="text-white">
                          <p>{{$reply->content}}</p>
                        </div>
                      </div>
                    @endforeach

                  @else
                    <!-- response -->
                    <div class="p-4 mt-4 rounded" style="background-color : rgba(0,0,0,0.3); ">
                      <p class="text-white">No Reply Yet!</p>
                    </div>
                  @endif
                  
                  



                  <!-- response -->
                  <!-- <div class="p-4 mt-4 rounded" style="background-color : rgba(0,0,0,0.3); ">
                    user header
                    <div class="flex items-start mb-6">
                      <div class="flex">
                        <div>
                          <p class="text-xs text-white mb-1">Post by</p>
                          <p class="text-white tracking-wide font-bold text-md" >Anonymous</p>
                        </div>
                      </div>
                      <p class="text-xs text-grey-lighter flex-1 text-right">about 14 hours ago</p>
                    </div>
                  
                    user content
                    <div class="text-white">
                      <p>
                        Piaraan kamu bisa gak berenang?
                      </p>
                      <p>(83/365❤)</p>
                    </div>
                  </div> -->

                </div>

                <!-- right -->
                <div class="flex-1 ml-2">
                    
                </div>
            </div>
        </main>



        <div v-show="modal" @click.self="toggleModal" class="modal invisible fixed z-50 pin overflow-hidden flex" style="background-color: rgba(31,21,20,0.8);">
          <div class="animated fadeInUp fixed shadow-inner max-w-md md:relative pin-b pin-x align-top m-auto justify-end md:justify-center p-8 bg-white md:rounded w-full md:h-auto md:shadow flex flex-col">
            <p class="">Membalas Pertanyaan</p>
            <p class="mb-2 text-xs text-grey-darker">
                in “Pernah gak sih ngerasain tiba2 sedih,galau,pengannya nangis aja gitu. Tapi gak ada terjadi hal apa2 . Yang kamu lakuin apa?”
            </p>
            <form action="">
                <textarea placeholder="Replying" rows="2" class="w-full border rounded p-2"></textarea>
                 <div class="flex justify-between items-center mt-2">
                     <!-- Rounded switch -->
                     <div class="flex items-center">
                         <label class="switch mr-2">
                           <input type="checkbox">
                           <span class="slider round"></span>

                         </label>
                         <span class="text-xs text-black">Ask anonymously</span>
                     </div>
                     

                     <button class="text-white w-1/6 bg-red p-1 rounded flex justify-center items-center">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 10L0 20V0zm0 8v4l10-2L0 8z"/></svg>
                     </button>
                 </div>
            </form>
            <span @click="toggleModal" class="absolute pin-t pin-r pt-4 px-4">
                <svg class="h-6 w-6 text-grey hover:text-grey-darkest" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
            </span>
          </div>
        </div>




    </div>
    
@endsection



