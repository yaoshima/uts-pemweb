<!DOCTYPE html>
<html lang="en">
<head>
	<title>Laravel</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="{{ URL::asset('css/auth_util.css') }}"> 
	<link rel="stylesheet" href="{{ URL::asset('css/auth_main.css') }}">
</head>
<body>
	@if(count($errors->all()))
		<div class="alert alert-success" role="alert">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
			</ul>			  
		</div>
	@endif
	<div class="limiter">
		<div class="">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="{{route('create.register')}}" method="POST">
					{{csrf_field()}}
					<span class="login100-form-title p-b-34">
						Account REGISTER
					</span>
					
					<div class="wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="first-name" class="input100" type="text" name="fullname" placeholder="Full Name">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="first-name" class="input100" type="text" name="username" placeholder="Userame">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input id="first-name" class="input100" type="email" name="email" placeholder="Email">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="password_confirmation" placeholder="Confirm Password">
						<span class="focus-input100"></span>
					</div>
					
					<div class="container-login100-form-btn" href="index.html">
						<button class="login100-form-btn">
							REGISTER
						</button>
					</div>

					<div class="w-full m-t-20 m-b-60 text-center">
						<a class="txt3" href="{{route('show.login')}}">
							LOGIN
						</a>
					</div>
				</form>
				<div class="login100-more" style="background-image: url('images/poto3.png');"></div> 
			</div>
		</div>
	</div>
	
	



</body>
</html>