@extends('layout.master')

@section('main')
    <div id="app" class="min-h-screen" style="background-color: #334d50;">
        @include('partials.nav')
        
        <main class="container mt-10">
            <!-- user header -->
            <div class="flex items-center mb-12">
              <img src="https://placekitten.com/80/80"  class="w-1/8 shadow-outline rounded-full mr-1">
              <div class="ml-5">
                <p class="text-xs text-white mb-1">@ {{$user->username}}</p>
                <p class="text-white tracking-wide font-bold mb-4 text-xl" >{{$user->fullname}}</p>
                <div class="flex items-center text-grey">
                  <div @click="follow">
                    <template v-if="isFollowed">
                      <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zm12.73-1.41A8 8 0 1 0 4.34 4.34a8 8 0 0 0 11.32 11.32zM6.7 9.29L9 11.6l4.3-4.3 1.4 1.42L9 14.4l-3.7-3.7 1.4-1.42z"/></svg>
                    </template>
                    <template v-else>
                      <button class="bg-black hover:bg-red text-white font-bold py-1 px-4 rounded-full">
                        
                        <svg class="fill-current h-3 w-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M2 6H0v2h2v2h2V8h2V6H4V4H2v2zm7 0a3 3 0 0 1 6 0v2a3 3 0 0 1-6 0V6zm11 9.14A15.93 15.93 0 0 0 12 13c-2.91 0-5.65.78-8 2.14V18h16v-2.86z"/></svg> 
                        <span class="text-xs">Follow</span>
                      </button>
                    </template>
                    

                    
                  </div>
                  

                  <svg class="cursor-pointer mx-2 fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M4 12a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm6 0a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm6 0a2 2 0 1 1 0-4 2 2 0 0 1 0 4z"/></svg>
                </div>
              </div>
            </div>
            <div class="flex">
                <!-- left -->
                <div class="w-3/5 mr-2">
                    

                    <!-- post card -->
                    <div class="bg-white rounded p-4">
                        <!-- question -->
                        <div class="mb-2">
                            <p class="inline post-card-text  text-lg">ngapain sih kalau abis putus ama pacar?</p>
                            <!-- sender -->
                            <span class="inline-flex items-center">
                                <img src="https://placekitten.com/15/15"  class="rounded-full mr-1">
                                <span class="text-xs">Billy</span>
                            </span>
                        </div>
                        
                        
                        <!-- answer -->
                        <div class="mt-4">
                            <!-- recipient -->
                            <div class="flex">
                               <img src="https://via.placeholder.com/30" class="rounded-full mr-2" style="height: 30px; width: 30px;">
                               <div class="">
                                   <p class="text-xs">aisyahqaulan</p> 
                                   <p class="text-grey text-xs">about 1 hour ago</p>
                               </div>
                            </div>
                            <!-- response -->
                            <p class="mt-4">
                                Tenang, aku masih punya kancing dan penghapus untuk mengisi pg serta khalayan indah untuk mengisi essay
                            </p>
                            
                        </div>

                        <!-- bottom menubar -->
                        <div class="flex  mt-4">
                            <div class="text-red pr-4 flex items-center">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 3.22l-.61-.6a5.5 5.5 0 0 0-7.78 7.77L10 18.78l8.39-8.4a5.5 5.5 0 0 0-7.78-7.77l-.61.61z"/></svg>
                                <span class="ml-1 text-red">7</span>
                            </div>
                            <div class="text-grey cursor-pointer" @click="toggleModal">
                                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 10L0 20V0zm0 8v4l10-2L0 8z"/></svg>
                            </div>
                        </div>

                        <hr class="border-t mx-2 border-grey-ligght">
                        <!-- more -->
                        <div class="flex justify-between">
                            <p class="text-xs text-red-light">+1 more answer</p>
                            <p class="text-xs text-grey">Read more</p>
                        </div>
                    </div>


                </div>

                <!-- right -->
                <div class="flex-1 ml-2">
                    <!-- user activity -->
                    <div class="flex justify-start">
                      <!-- post -->
                      <div class=" text-white flex px-1">
                        <div class="rounded-full h-8 w-8 flex items-center justify-center bg-blue">
                          <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 15l-4 4v-4H2a2 2 0 0 1-2-2V3c0-1.1.9-2 2-2h16a2 2 0 0 1 2 2v10a2 2 0 0 1-2 2h-8zM5 7v2h2V7H5zm4 0v2h2V7H9zm4 0v2h2V7h-2z"/></svg>
                        </div>
                        <div class="mx-2">
                          <p class="font-bold">{{$postCount}}</p>
                          <p class="text-grey-lighter text-xs">Posts</p>
                        </div>
                        
                      </div>

                      <!-- likes -->
                      <div class=" text-white flex px-1">
                        <div class="rounded-full h-8 w-8 flex items-center justify-center bg-red">
                          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" class="fill-current h-4 w-4"><path d="M10 3.22l-.61-.6a5.5 5.5 0 0 0-7.78 7.77L10 18.78l8.39-8.4a5.5 5.5 0 0 0-7.78-7.77l-.61.61z"></path></svg>
                        </div>
                        <div class="mx-2">
                          <p class="font-bold">{{$likeCount}}</p>
                          <p class="text-grey-lighter text-xs">Likes</p>
                        </div>
                      </div>
                    </div>

                    <!-- user bio -->
                    <div class="mt-8">
                      <!-- title -->
                      <div class="flex justify-between mb-3">
                        <p class="text-white">About {{$user->fullname}}:</p>
                        <p class="text-red-light cursor-pointer text-xs font-bold">
                          <svg class="fill-current h-3 w-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.3 3.7l4 4L4 20H0v-4L12.3 3.7zm1.4-1.4L16 0l4 4-2.3 2.3-4-4z"/></svg>
                          <a class="no-underline" style="color: inherit;" href="{{route('edit')}}">Edit Profile</a>
                        </p>
                      </div>
                      <!-- detail -->
                      <div class="user-bio">
                        <!-- bio -->
                        <div class="flex items-center mb-2">
                          <svg class="fill-current h-3 w-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M5 5a5 5 0 0 1 10 0v2A5 5 0 0 1 5 7V5zM0 16.68A19.9 19.9 0 0 1 10 14c3.64 0 7.06.97 10 2.68V20H0v-3.32z"/></svg>
                          <p class="mx-3 text-xs">
                            {{$user->bio ?? "Please tell about yourself!"}}
                          </p>
                        </div> 

                        <!-- Location -->
                        <div class="flex items-center">

                          <svg class="fill-current h-3 w-3" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20S3 10.87 3 7a7 7 0 1 1 14 0c0 3.87-7 13-7 13zm0-11a2 2 0 1 0 0-4 2 2 0 0 0 0 4z"/></svg>
                          <p class="mx-3 text-xs">
                            {{$user->location ?? "Where is your location?"}}
                          </p>
                        </div> 
                        
                      </div>
                      
                    </div>
                </div>
            </div>
        </main>



        <div v-show="modal" @click.self="toggleModal" class="modal invisible fixed z-50 pin overflow-hidden flex" style="background-color: rgba(31,21,20,0.8);">
          <div class="animated fadeInUp fixed shadow-inner max-w-md md:relative pin-b pin-x align-top m-auto justify-end md:justify-center p-8 bg-white md:rounded w-full md:h-auto md:shadow flex flex-col">
            <p class="">Membalas Pertanyaan</p>
            <p class="mb-2 text-xs text-grey-darker">
                in “Pernah gak sih ngerasain tiba2 sedih,galau,pengannya nangis aja gitu. Tapi gak ada terjadi hal apa2 . Yang kamu lakuin apa?”
            </p>
            <form action="">
                <textarea placeholder="Replying" rows="2" class="w-full border rounded p-2"></textarea>
                 <div class="flex justify-between items-center mt-2">
                     <!-- Rounded switch -->
                     <div class="flex items-center">
                         <label class="switch mr-2">
                           <input type="checkbox">
                           <span class="slider round"></span>

                         </label>
                         <span class="text-xs text-black">Ask anonymously</span>
                     </div>
                     

                     <button class="text-white w-1/6 bg-red p-1 rounded flex justify-center items-center">
                        <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M0 0l20 10L0 20V0zm0 8v4l10-2L0 8z"/></svg>
                     </button>
                 </div>
            </form>
            <span @click="toggleModal" class="absolute pin-t pin-r pt-4 px-4">
                <svg class="h-6 w-6 text-grey hover:text-grey-darkest" role="button" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><title>Close</title><path d="M14.348 14.849a1.2 1.2 0 0 1-1.697 0L10 11.819l-2.651 3.029a1.2 1.2 0 1 1-1.697-1.697l2.758-3.15-2.759-3.152a1.2 1.2 0 1 1 1.697-1.697L10 8.183l2.651-3.031a1.2 1.2 0 1 1 1.697 1.697l-2.758 3.152 2.758 3.15a1.2 1.2 0 0 1 0 1.698z"/></svg>
            </span>
          </div>
        </div>




    </div>
    
@endsection



