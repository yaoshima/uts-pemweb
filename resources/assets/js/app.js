
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

new Vue({
  el: '#app',
  data: {
    isDropdownOpen: false,
    message: 'Hello Vue!',
    modal: false,
    isFollowed : false
  },
  methods : {
    toggleDropdown(){
    	this.isDropdownOpen = !this.isDropdownOpen;
    	$(".dropdown-item").removeClass('invisible');
    },
    toggleModal($parent) {
      this.modal = !this.modal
      $(".modal").removeClass('invisible');
      $("#parent").val($parent);
    },
    follow(){
      this.isFollowed = !this.isFollowed
    }
  }
});

