<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('user/{user}', 'UserController@show')->name('show.user');

Route::get('post/{post}', 'PostController@show')->name('show.post');

Route::put('profile/edit/{user}', 'UserController@update')->name('update.profile');
Route::get('profile/edit', 'UserController@edit')->name('edit');


// auth
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('show.register');
Route::post('register', 'Auth\RegisterController@register')->name('create.register');

Route::get('login', 'Auth\LoginController@showLoginForm')->name('show.login');
Route::post('login', 'Auth\LoginController@login')->name('process.login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware(['auth'])->group(function() {
	Route::get('/', 'PostController@index')->name('home');

	Route::post('/post', 'PostController@create')->name('create.post');
	Route::post('/reply', 'PostController@reply')->name('create.reply');
});