<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['user_id', 'content', 'anonymous' , 'parent'];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function replies(){
    	return $this->hasMany('App\Post','parent');
    }

    public function parent(){
    	return $this->belongsTo('App\Post','parent');
    }

}
