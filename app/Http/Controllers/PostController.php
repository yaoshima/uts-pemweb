<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Auth;

class PostController extends Controller
{   
    public function index(){    
        $posts = Post::all()->where('parent',null);
        return view('home', compact('posts'));
    }

    public function show(Post $post){
        return view('post_detail', compact('post'));
    }

    public function create(Request $request){    
    	$userId = Auth::id();
    	$isAnonymous = $request->has('anonymous');
        $this->validate($request, [
                'content' => 'required'
            ]);
        $post = new Post([
        		'content' => $request->content, 
        		'user_id' => $userId,
        		'anonymous' => $isAnonymous
        	]);
        $post->save();
        return redirect()->back();
    }

    public function reply(Request $request){    
    	$userId = Auth::id();
    	$isAnonymous = $request->has('anonymous');
        $this->validate($request, [
                'content' => 'required',
                'parent' => 'required'
            ]);
        $post = new Post([
        		'content' => $request->content, 
        		'user_id' => $userId,
        		'anonymous' => $isAnonymous
        	]);
        $post->parent = $request->parent;
    	$post->save();
        return redirect()->back();
    }
}
