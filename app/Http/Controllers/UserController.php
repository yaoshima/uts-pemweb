<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class UserController extends Controller
{
    

    public function show(User $user){
    	$likeCount = $user->likes->count();
    	$postCount = $user->posts->count();
    	return view('profile', compact('user','likeCount', 'postCount'));
    }

	public function edit(){
		$user = Auth::user();
    	return view('edit', compact('user'));
    }

    public function update(Request $request, User $user){
        $newData = $request->all();
        $user->fill($newData);
        $user->save();



        return redirect()->route('show.user',['user' => $user->username]);
    } 
}
